#!/bin/bash
#Paths are for Centos 7.x

# Install optional packages
/usr/bin/yum install -y epel-release
/usr/bin/yum install -y bash-completion htop yum-utils dkms open-vm-tools

# Move the blank nic config back
CONF_DIR="/etc/sysconfig/network-scripts"
NIC="ens192"
cp "${CONF_DIR}/ifcfg-${NIC}.blank" "${CONF_DIR}/ifcfg-${NIC}"

#stop logging services
/sbin/service rsyslog stop
/sbin/service auditd stop

#remove old kernels
/usr/bin/package-cleanup --oldkernels --count=1

#clean yum cache
/usr/bin/yum clean all

#force logrotate to shrink logspace and remove old logs as well as truncate logs
/usr/sbin/logrotate -f /etc/logrotate.conf
/bin/rm -f /var/log/*-???????? /var/log/*.gz
/bin/rm -f /var/log/dmesg.old
/bin/rm -rf /var/log/anaconda
/bin/cat /dev/null > /var/log/audit/audit.log
/bin/cat /dev/null > /var/log/wtmp
/bin/cat /dev/null > /var/log/lastlog
/bin/cat /dev/null > /var/log/grubby

#remove udev hardware rules
/bin/rm -f /etc/udev/rules.d/70*

#remove uuid from ifcfg scripts
/bin/sed -i '/^(HWADDR|UUID)=/d' /etc/sysconfig/network-scripts/ifcfg-ens192

#remove SSH host keys
/bin/rm -f /etc/ssh/*key*

# Output the CentOS release
cat /etc/redhat-release

#remove root users shell history
/bin/rm -f ~root/.bash_history
unset HISTFILE

#remove root users SSH history
/bin/rm -rf ~root/.ssh/
