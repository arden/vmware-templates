#!/bin/bash
# Paths are for Rocky 9.x

# Enable CRB (CodeReady Builder)
/usr/bin/crb enable

# Install optional packages
/usr/bin/dnf install -y epel-release
/usr/bin/dnf install -y bash-completion htop dkms open-vm-tools vim nmon git bind-utils traceroute tcpdump

# Cleanup the network settings
SAMPLE_DIR="/root/vmware-templates/rocky9/etc/NetworkManager/system-connections"
CONF_DIR="/etc/NetworkManager/system-connections"
NIC="ens192"
cp "${SAMPLE_DIR}/${NIC}.nmconnection.blank" "${CONF_DIR}/${NIC}.nmconnection"
/usr/bin/find "${CONF_DIR}" -type f -exec chmod 600 {} \;

# stop logging services
/usr/bin/systemctl stop systemd-journald

# remove old kernels
/usr/bin/dnf remove -y --oldinstallonly

# clean dnf cache
/usr/bin/dnf clean all

# force logrotate to shrink logspace and remove old logs as well as truncate logs
/usr/sbin/logrotate -f /etc/logrotate.conf
/bin/rm -f /var/log/*-???????? /var/log/*.gz
/bin/rm -f /var/log/dmesg.old
/bin/rm -rf /var/log/anaconda
/bin/cat /dev/null > /var/log/audit/audit.log
/bin/cat /dev/null > /var/log/wtmp
/bin/cat /dev/null > /var/log/lastlog
/bin/cat /dev/null > /var/log/grubby

# remove udev hardware rules
/bin/rm -f /etc/udev/rules.d/70*

# remove the machine ID
# On rocky 9 the file has to exist, but be empty for things to work out
/bin/cat /dev/null /etc/machine-id

# remove SSH host keys
/bin/rm -f /etc/ssh/*key*

# Output the Rocky release
/bin/cat /etc/rocky-release

# remove root users shell history
/bin/rm -f ~root/.bash_history
unset HISTFILE

# remove root users SSH history
/bin/rm -rf ~root/.ssh/

